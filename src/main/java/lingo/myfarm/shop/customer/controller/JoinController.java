package lingo.myfarm.shop.customer.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;


import lingo.myfarm.shop.customer.vo.Customer;

@Controller
@RequestMapping(value="customer")
public class JoinController {
	
	private static final Logger logger = LoggerFactory.getLogger(JoinController.class);
	@Autowired
	lingo.myfarm.shop.customer.dao.CustomerDAO dao;
	
	
	@RequestMapping(value="jointohome", method=RequestMethod.GET)
	public String home(){
		
		return "redirect:/";
	}
	
	@RequestMapping(value="joinForm", method=RequestMethod.GET)
	public String joinForm(Model model){
		logger.info("joinForm 이동시작");
		
		//빈 객체 준비
		Customer customer = new Customer();
		//빈 객체를 다음 페이지에 넘겨주고 빈 객체의 유무 정상경로 접근확인
		model.addAttribute("customer",customer);
		
		logger.info("joinForm 이동종료");
		return "customer/joinForm";
	}
	
	@ResponseBody
	@RequestMapping(value="idSearch", method=RequestMethod.GET)
	public boolean idSearch(@RequestBody String username){
		logger.info("id 중복확인 시작");
		
		Customer customer = dao.customerSearch(username);
		
		System.out.println(customer);
		logger.info("id 중복확인 종료");
		if(customer==null){
			return true;	
			
		}else{
			return false;
		}
	}
	
	
	@RequestMapping(value="join", method= RequestMethod.POST)
	public String join(@ModelAttribute("customer") Customer customer,
			Model model){
		
		int result =dao.customerJoin(customer);
				
		if(result!=1){
			//등록실패의 경우 : query가 한번도 실행되지 않았다는 것
			model.addAttribute("errorMsg","등록실패");
			logger.info("회원등록실패!");
			return "customer/joinForm";
		}
		
		
		logger.info("회원 등록 종료");
		
		return "redirect:joinSuccess";
	}
	
	@RequestMapping(value="joinSuccess", method=RequestMethod.GET)
	public String joinComplete(SessionStatus session
			,Model model
			,@ModelAttribute("customer") Customer customer){
		//다른사람이 이 경로로 못들어 오게 하기 위해서 다시한번 @ModelAttribute 사용
		logger.info("회원 가입 성공 폼 이동시작");
		model.addAttribute("id", customer.getUsername());
		//Session초기화
		session.setComplete();
		logger.info("회원 가입 성공 폼 이동 종료");
		
		return "customer/joinSuccess";
	}


}
