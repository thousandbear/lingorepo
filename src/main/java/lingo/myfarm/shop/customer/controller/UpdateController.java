package lingo.myfarm.shop.customer.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import lingo.myfarm.shop.customer.dao.CustomerDAO;
import lingo.myfarm.shop.customer.vo.Customer;


@RequestMapping(value="customer")
@Controller
public class UpdateController{
	
	private static final Logger logger = LoggerFactory.getLogger(UpdateController.class);
	
	@Inject
	CustomerDAO dao;
	
	@RequestMapping(value="updatetohome", method=RequestMethod.GET)
	public String home(){
		
		return "redirect:/";
	}
	
	@RequestMapping(value ="updateForm", method=RequestMethod.GET)
	public String updateForm(HttpSession session, Model model){
		logger.info("회원정보 업데이트 폼 이동 시작");
		String id = (String)session.getAttribute("custid");
		Customer customer = dao.customerSearch(id);
		model.addAttribute("customer", customer);
		logger.info("회원 정보 업데이트 폼 이동 종료");
		return "customer/updateForm";
	}
	
	@RequestMapping(value="update", method=RequestMethod.POST)
	public String update(@ModelAttribute("customer") Customer customer,
			 Model model,HttpSession session){
		String id = (String)session.getAttribute("username");
		customer.setUsername(id);
		int result = dao.customerUpdate(customer);
		if(result!=1){
			model.addAttribute("errorMsg","수정실패");
			return "customer/updateForm";
		}
		return "redirect:updateComplete";
	}
	
	@RequestMapping(value="updateComplete", method =RequestMethod.GET)
	public String updateComplete(SessionStatus status, Model model,
			@ModelAttribute("customer") Customer customer, HttpSession session){
		
		
		model.addAttribute("custid",customer.getUsername());
		status.setComplete();
		
		return "customer/updateComplete";
	}

}
