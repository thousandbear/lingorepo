package lingo.myfarm.shop.customer.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import lingo.myfarm.shop.customer.dao.CustomerDAO;
import lingo.myfarm.shop.customer.vo.Customer;



@RequestMapping(value="customer")
@Controller
public class LoginController {
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	CustomerDAO dao;
	
	@RequestMapping(value="logintohome", method=RequestMethod.GET)
	public String home(){
		
		return "redirect:/";
	}
	
	@RequestMapping(value="loginForm", method=RequestMethod.GET)
	public String loginForm(){
		logger.info("로그인 폼 이동 시작");
		
		logger.info("로그인 폼 이동 종료");
		return "customer/loginForm";
	}
	

	@RequestMapping(value="login", method=RequestMethod.POST)
	public String login(Customer customer, HttpSession session
			,Model model){
		logger.info("로그인 시작");
		Customer pc = dao.customerSearch(customer.getUsername());
		if(pc!=null&&pc.getPassword().equals(customer.getPassword())){
			//로그인 성공
			session.setAttribute("custid", pc.getUsername());
			session.setAttribute("custname", pc.getUsername());
			
							
		}else{
			model.addAttribute("failMsg", "로그인에 실패하였습니다");
			return "customer/loginForm";			
		}
				
		
		
		logger.info("로그인 종료");
		return "redirect:/";
	}
	
	@RequestMapping(value="logout", method=RequestMethod.GET)
	public String logout(HttpSession session){
		logger.info("로그아웃 시작");
		session.removeAttribute("custid");
		session.removeAttribute("custname");
		logger.info("로그아웃 종료");
		
		return "redirect:/";		
	}
	
	

}
