package lingo.myfarm.shop.customer.dao;

import lingo.myfarm.shop.customer.vo.Customer;

public interface CustomerMapper {
	
		//회원 정보 검색
		public Customer customerSearch(String username);
		
		//회원 정보 등록
		public int customerJoin(Customer customer);
		
		//회원 정보 변경
		public int customerUpdate(Customer customer);
		
		

}
