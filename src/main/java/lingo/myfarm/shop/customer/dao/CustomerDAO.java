package lingo.myfarm.shop.customer.dao;


import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lingo.myfarm.shop.customer.vo.Customer;


@Repository
public class CustomerDAO {
	
	@Autowired
	SqlSession sqlsession;
	
			//회원 정보 검색
			public Customer customerSearch(String username) {
				Customer customer = null;
				
				CustomerMapper mapper = sqlsession.getMapper(CustomerMapper.class);
				try {
					customer = mapper.customerSearch(username);
				
				}catch(Exception e) {
					e.printStackTrace();
				}
				
					return customer;
				}
			
			//회원 정보 등록
			public int customerJoin(Customer customer) {
				int result =0;
				CustomerMapper mapper = sqlsession.getMapper(CustomerMapper.class);
				try {
					result = mapper.customerJoin(customer);
					
				}catch(Exception e) {
					e.printStackTrace();
				}			
				
				return result;
			}
			
			//회원 정보 변경
			public int customerUpdate(Customer customer) {
				int result =0;
				
				CustomerMapper mapper = sqlsession.getMapper(CustomerMapper.class);
				try {
					result = mapper.customerUpdate(customer);
				}catch(Exception e) {
					e.printStackTrace();
				}
				
				return result;				
			}

}
